# Frequently-Used Links and Articles #
*  C++ Optimization
  *  [Optimizing C++ Wikibook](https://en.wikibooks.org/wiki/Optimizing_C%2B%2B)
* Hash Functions
  * [Collision Probabilities](http://preshing.com/20110504/hash-collision-probabilities/)
  * [Hash Functions for Strings](http://stackoverflow.com/questions/114085/fast-string-hashing-algorithm-with-low-collision-rates-with-32-bit-integer)
     * [FNV Variants](http://isthe.com/chongo/tech/comp/fnv/)
* Windows / Xbox Programming
  * [Windows Programming WIkibook](https://en.wikibooks.org/wiki/Category:Windows_Programming)
  * [Xbox One XDK Documentation](https://developer.xboxlive.com/en-us/platform/development/documentation/Pages/home.aspx)
  * [Win32 Data Types](https://msdn.microsoft.com/en-ca/library/windows/desktop/aa383751%28v=vs.85%29.aspx)
* Unreal Engine
  * [Unreal Developer Network](https://udn.unrealengine.com/index.html)
  * [UE4 Documentation](https://udn.unrealengine.com/docs/ue4/INT/index.html)
* Concurrency
  * [Traditional Double-Checked Lock](http://www.aristeia.com/Papers/DDJ_Jul_Aug_2004_revised.pdf)
  * [C++11 Double-Checked Lock](http://preshing.com/20130930/double-checked-locking-is-fixed-in-cpp11/)
  * [Lock Usage](http://preshing.com/20111118/locks-arent-slow-lock-contention-is/)
  * [Fences](http://preshing.com/20130922/acquire-and-release-fences/)
* C++11 Concepts
  * Move Semantics
     * [Rule of Zero pt. 1](https://blog.feabhas.com/2015/01/the-rule-of-zero/)
     * [Rule of Zero pt. 2](https://blog.feabhas.com/2015/11/becoming-a-rule-of-zero-hero/)
* C++ Concepts
  * Templates
     * [Detecting Methods using SFINAE](http://blog.quasardb.net/sfinae-hell-detecting-template-methods/)
  * SOLID Design Principle
     * [Wikipedia](https://en.wikipedia.org/wiki/SOLID_%28object-oriented_design%29)
     * [Article and PDFs](http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod)
